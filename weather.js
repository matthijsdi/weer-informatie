var city = null;
$(document).ready(function () {
    hideStuff()
    $('#submit').click(function () {
        city = $("#city").val();

        if (city != '') {
            showStuff();
            todayAjax();
            forecastAjax();
            tempYesterday();

        } else {
            $('#error').html('Field cannot be empty');
        }
    });
});

function todayAjax(){
    $.ajax({
        url: "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric" + "&APPID=2540e7a7a96b3ff958368abfbadb6654",
        type: "GET",
        dataType: "jsonp",
        success: function (data) {
        
        var icon = "img/" + data.weather[0].icon + ".png";
        $('#a1').css('background-image', 'url(' + icon + ')');
        
        var tempToday = "<h1>Vandaag is het "+ data.main.temp +" graden" + "<br> met een maximale temperatuur van " + data.main.temp_max + " graden en een minimale temperatuur van " + data.main.temp_min + " graden.</h1>";
        $("#a2").html(tempToday);

        var cloud = "<h1>Het is vandaag voor " + data.clouds.all + "% bewolkt.</h1>"
        $("#a3").html(cloud);

        var wind = "<h1>De windsnelheid is vandaag " + data.wind.speed + " m/s en heeft een richting van " + data.wind.deg + " graden t.o.v. de noordpool."
        $("#a5").html(wind);
        }
    });
}

function forecastAjax() {
    $.ajax({
        url: "https://api.openweathermap.org/data/2.5/forecast?q=" + city + "&units=metric" + "&APPID=2540e7a7a96b3ff958368abfbadb6654",
        type: "GET",
        dataType: "jsonp",
        success: function (temp) {
            var tempVar = tempTomorrow(temp);
            $("#a4").html(tempVar);
        },
    });
}

function tempTomorrow(temp) {
    return "<h1>Morgen wordt het " + temp.list[0].main.temp + " graden.<br>" + "Overmorgen wordt het " + temp.list[1].main.temp + " graden.</h1>";
}

function tempYesterday(){
    $.ajax({
        url: "https://history.openweathermap.org/data/2.5/history/city?q=" + city + "&units=metric" + "&APPID=2540e7a7a96b3ff958368abfbadb6654",
        type: "GET",
        dataType: "jsonp",
        success: function (hist) {
            var histVar = yesterdayTemp(hist);
            $("#a5").html(histVar);
        },
        
    })
}

function yesterdayTemp(hist){
    return "<h1>Gisteren was het " + hist.list[0].main.temp + " graden en eergisteren was het " + hist.list[1].main.temp + " graden.</h1>" ;
}


function hideStuff() {
    $('.a1').css('display', 'none');
}

function showStuff() {
    $('.a1').css('display', 'inline');
}

function appear(){
    $('.a1').css('display', 'inline');
    $('.a1').animateCss('bounceIn')
}
